<?php

use Illuminate\Database\Seeder;
use App\Models\Driver;

class DriversTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::beginTransaction();
      try{
        factory(App\Models\Driver::class, rand(5,5))->create();
        DB::commit();
      }catch(Exception $e){
        DB::rollBack();
        echo "\n** Seeding error: ".get_class($this)." in line:".$e->getLine()." **\n";
        throw $e;
      }
    }
}
