<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::beginTransaction();
      try{
        factory(App\Models\User::class, rand(20, 20))->create();
        DB::commit();
      }catch(Exception $e){
        DB::rollBack();
        echo "\n** Seeding error: ".get_class($this)." in line:".$e->getLine()." **\n";
        throw $e;
      }
    }
}
