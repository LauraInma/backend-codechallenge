<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Driver;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Driver::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'driver_license' => $faker->unique()->safeEmail,
        'api_token' => md5(rand()),
    ];
});
