<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
   /**
   * Driver has may tasks
   */
  public function tasks(){
      return $this->hasMany('App\Models\Order');
  }
}
