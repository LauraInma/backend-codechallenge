<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'name', 'surname', 'email', 'phone_number', 'address', 'date'
    ];

    /**
    * Order belongs to driver
    */
    public function driver(){
        return $this->belongsTo('App\Models\Driver');
    }
}
