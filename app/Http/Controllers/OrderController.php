<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Driver;
use App\Models\Order;
use App\Models\User;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = null;
        
        $user = User::where('api_token', $request->input('api_token'))->first();
        
        if(!$request->input('api_token') || !$user ){
            $message = 'Invalid user'; 
            return response()->json($message, 401);    
        }

        $order = new Order();
        $order->name = $user->fullName();
        $order->email = $user->email;
        $order->phone_number = $user->phone_number;
        $order->address = $user->address;
        $order->date = $request->input('date');
        $order->time_range = $this->setRange($request->input('from'), $request->input('to'));

        if($order->save()){
            $driver = Driver::inRandomOrder()->first();
            $driver->tasks()->save($order);
            $message = 'Order saved!';
            return response()->json($message, 200);    
        }else{
            $message = 'Something happened. Please try again later.';
            return response()->json($message, 500);    
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Validates and returns the time range as a string.
     * Assuming an a 24 hour format input (i.e. 10:00 - 16:30)
     *
     * @param  str  $from  -  Start hour
     * @param  str  $to  -  End hour
     * @return \Illuminate\Http\Response
     */
    private function setRange($from, $to)
    {
        $range = json_encode(round((strtotime($to) - strtotime($from))/3600, 1));
        if($range < 1 || $range > 8){
            return json_encode('Invalid time range');
        }
        $timeRange = $from.' - '.$to;
        return $timeRange;
    }
}
